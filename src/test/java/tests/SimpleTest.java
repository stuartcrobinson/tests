package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;

public class SimpleTest {

    @BeforeClass
    public void setUp() {
        // code that will be invoked when this test is instantiated
        System.setProperty("webdriver.chrome.driver", new File("chromedriver2_29").getAbsolutePath());

    }

    @Test(groups = { "fast" })
    public void aFastTest2() {
        System.out.println("Fast test2");
    }

    @Test(groups = { "slow2" })
    public void aSlowTest2() {
        System.out.println("Slow test2");
    }

}
