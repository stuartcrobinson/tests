package tests;

import org.openqa.selenium.By;
import utilities.ned.MyLogger;
import utilities.UiTestClass;
import org.testng.Reporter;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class Simple2Test extends UiTestClass{


    @Test(groups = {"fast"})
    public void aFastTest() {

        UiTestClass.hi();

        System.out.println("Fast test!!");

        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");

//
//        System.setProperty("webdriver.chrome.driver", new File("chromedriver2_29").getAbsolutePath());
//        WebDriver driver;
//
//        driver = new ChromeDriver();

        driver.get("https://www.estimize.com/calendar");
        System.out.println(driver.getTitle());


        driver.findElement(By.xpath("//*[@id='week-nav']//a[@class='next']")).click();

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.findElement(By.xpath("//*[@id='week-nav']//a[@class='next']")).click();

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.findElement(By.xpath("//*[@id='week-nav']//a[@class='next']")).click();

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.findElement(By.xpath("//*[@id='week-nav']//a[@class='next']")).click();

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //

        //li[@class='day']/a


        driver.findElement(By.xpath("//li[@class='day']/a")).click();

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(driver.getTitle());
        System.out.println(driver.getPageSource());

        MyLogger.logScreenShot(driver);
//
//        Reporter.log("<br><img src='imageslalala/screenShot.png' />", 0, true);
//        Reporter.log("Saved <a href=" + "filename" + ">Saved Screenshot</a>");
//
//        System.setProperty("org.uncommons.reportng.escape-output", "false");
//
//        String fileName = "awefakwef";
//
//        Reporter.log(
//                "<a title= \"title\" href=\"../path/from/target/" + fileName + "\">" +
//                        "<img width=\"418\" height=\"240\" alt=\"alternativeName\" title=\"title\" src=\"../surefire-reports/html/screenShots/" + fileName + "\"></a>");



        method1();

    }

    private void method1() {
        method2();
    }

    private void method2() {
        assertTrue(false);

    }

    @Test(groups = {"slow"})
    public void aSlowTest() {
        System.out.println("Slow test!!");

//        try {
//            Thread.sleep(10000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
//
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");
        Reporter.log("lalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \nlalalala \n");


// /
    }

}
