package utilities;

import com.google.common.collect.ImmutableMap;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
//import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.IOException;

public class UiTestClass {
    public WebDriver driver;
    private static ChromeDriverService service;

    @BeforeClass
    public void parentSetup() throws IOException {

        System.setProperty("webdriver.chrome.driver", new File("chromedriver2_29").getAbsolutePath());

//        String osName = System.getProperty("os.name");
//
//        System.setProperty("webdriver.chrome.driver", new File("chromedriver2_29").getAbsolutePath());


        // Get the OS Type and browser
        String osName = System.getProperty("os.name");

        if (System.getenv("TERM_PROGRAM") != null && System.getenv("TERM_PROGRAM").equals("Apple_Terminal")) {
            System.setProperty("webdriver.chrome.driver", new File("chromedriver2_29").getAbsolutePath());

        } else {
            System.setProperty("webdriver.chrome.driver", new File("chromedriver_6_linux_2_30").getAbsolutePath());

            service = new ChromeDriverService.Builder()
                    .usingDriverExecutable(new File("chromedriver_6_linux_2_30"))
                    .usingAnyFreePort()

                    .withEnvironment(ImmutableMap.of("DISPLAY", ":1"))

                    .build();

            service.start();


//            driver = new RemoteWebDriver(service.getUrl(), DesiredCapabilities.chrome());

            driver = new ChromeDriver(service);
            driver.manage().window().setSize(new Dimension(1424, 968));

//
//            service = new ChromeDriverService.Builder()
//                    .usingChromeDriverExecutable(new File("/path/to/chromedriver"))
//                    .usingAnyFreePort()
//                    .withEnvironment(ImmutableMap.of("DISPLAY", ":20"))
//                    .build();

        }

//        if ("Linux".equals(osName)) {
//            chromedriver = BrobotFileUtils.getTestResourceFilePath("chromedriver_linux64");
//        }

////        String browser = BrobotProperties.current().browser();
//
////        if (browser.equals("chrome")) {
//            // Use Chrome Driver
//            String chromedriver = BrobotFileUtils.getTestResourceFilePath("chromedriver_mac2.29");
//            if ("Linux".equals(osName)) {
//                chromedriver = BrobotFileUtils.getTestResourceFilePath("chromedriver_linux64");
//            } else if (osName.contains("Windows")) {
//                chromedriver = BrobotFileUtils.getTestResourceFilePath("chromedriver_windows");
//            }
//
//            System.setProperty("webdriver.chrome.driver", chromedriver);
//            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//            ChromeOptions options = new ChromeOptions();
//
//            Map<String, String> mobileEmulation = new HashMap<String, String>();
//            mobileEmulation.put("deviceName", "Google Nexus 5");
//
//            options.setExperimentalOption("mobileEmulation", mobileEmulation);
//
//            // Enables test-type mode for ChromeDriver to force Chrome to ignore invalid command line flags.
//            options.addArguments("test-type");
//            capabilities.setCapability("chrome.binary", "chromedriver_mac2.29");
//            capabilities.setCapability(ChromeOptions.CAPABILITY, options);
//            return new ChromeDriver(capabilities);
//
//
//
//


        driver = new ChromeDriver();

    }

    @AfterClass
    public void parentAfterClass() {
        driver.quit();
    }

    public static void hi() {
        System.out.println("hi");
    }

    public static void main(String[] asdf) {
        System.out.println("hi");
    }
}
