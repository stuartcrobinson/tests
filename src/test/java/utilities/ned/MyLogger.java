package utilities.ned;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;


public class MyLogger {

    public static void logScreenShot(WebDriver driver) {

        String timestamp = LocalDateTime.now().toString();

        // Take screenshot and store as a file format
        File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            Files.createDirectories(new File("target/surefire-reports/images").toPath());
            FileUtils.copyFile(src, new File("target/surefire-reports/images/" + timestamp + ".png"));
            Reporter.log("HTMLESCAPEDELIMITER<img style='width:800px' src='images/" + timestamp + ".png' />HTMLESCAPEDELIMITER");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
