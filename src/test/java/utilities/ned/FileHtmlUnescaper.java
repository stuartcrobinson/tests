package utilities.ned;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.unbescape.html.HtmlEscape;


/**
 * Created by stuart.robinson on 6/29/17.
 */
public class FileHtmlUnescaper {


    public static void main(String[] awef) {

//        String toUn = "<br/>&lt;br&gt;HTMLESCAPEDELIMITER&lt;img src=&apos;images/screenShot.png&apos; /&gt;HTMLESCAPEDELIMITER<br/>&lt;br&gt;&lt;img src=&apos;imageslalala/screenShot.png&apos; /&gt;<br/>";
//
//        System.out.println(StringEscapeUtils.unescapeHtml3(toUn));  //sucks
//        System.out.println(HtmlEscape.unescapeHtml(toUn));
//
//        HtmlEscape.unescapeHtml(toUn);
//
////        StringEscapeUtils.un
//
//        System.exit(0);


        try {
            List<String> lines = Files.readAllLines(new File("target/surefire-reports/emailable-report.html").toPath());

            List<String> output = new ArrayList();

            for (String line : lines) {


                //        Reporter.log("<br>STARTHTMLESCAPE<img src='images/screenShot.png' />ENDHTMLESCAPE");


                System.out.println(line);

                String[] ar = line.split("HTMLESCAPEDELIMITER");

                if (ar.length < 3)
                    output.add(line);
                else {

                    String toUnescape = ar[1];

                    System.out.println(toUnescape);


                    toUnescape = HtmlEscape.unescapeHtml(toUnescape);

                    System.out.println(toUnescape);

                    line = ar[0] + toUnescape + ar[2];
                    System.out.println(line);

                    output.add(line);
                }
            }

            Files.write(new File("target/surefire-reports/emailable-report.html").toPath(), output);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
